package com.company;

public class Main {

    public static void main(String[] args) {
	Cipher AtBashCipher = new AtBashCipher();

	//Encode
	    System.out.println("Encode: "+ AtBashCipher.encode("Ala ma kota"));

	//Decode
        System.out.println("Decode: " + AtBashCipher.decode("Zoz nz plgz"));
    }
}
